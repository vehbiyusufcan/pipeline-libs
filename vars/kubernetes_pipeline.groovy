def call(body) {

 def config = [: ]
 body.resolveStrategy = Closure.DELEGATE_FIRST
 body.delegate = config
 body()

 def docker_builds = new org.foo.docker_build()
 def docker_push = new org.foo.docker_push()
 def kubernetes_deployment =  new org.foo.kubernetes_deployment()

 jenkins.branch.WorkspaceLocatorImpl.PATH_MAX = 2

 dockerImage = "${config.dockerRepository}/${config.solutionName}"
 dockerRepository = "${config.dockerRepository}"
 dockerRepoCredentialId = "${config.dockerRepoCredentialId}"
 KubernetesCredentialFilePath = "${config.KubernetesCredentialFilePath}"


 pipeline {
   agent {
     node{
       label config.jenkinsNode
     }
   }
   stages{

     stage('Docker Build'){
       steps{
         script{
           docker_builds.buildDockerImage(dockerImage)
         }
       }
     }

     stage('Push Docker Image'){
       steps{
         script{
              docker_push.pushDockerImage(dockerImage,dockerRepository,dockerRepoCredentialId)
         }
       }
     }

     stage('Deploy to Kubernetes Environment'){
       steps{
         script{
           kubernetes_deployment.deployToTestKubernetesEnvironment()
         }
       }
     }

   }

   post {
     always {
        deleteDir()
      }
       success {
         slackSend (color: "${env.SLACK_COLOR_GOOD}",
             channel: "${env.SLACK_CHANNEL_1}",
             message: "SUCCESS: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}\n")
       }
       failure {
         slackSend (color: "${env.SLACK_COLOR_DANGER}",
             channel: "${env.SLACK_CHANNEL_1}",
             message: "FAIL: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}\n")
       }
       unstable {
         slackSend (color: "${env.SLACK_COLOR_WARNING}",
             channel: "${env.SLACK_CHANNEL_1}",
             message: "UNSTABLE: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}\n")
       }
       aborted {
         slackSend (color: "${env.SLACK_COLOR_WARNING}",
             channel: "${env.SLACK_CHANNEL_1}",
             message: "ABORTED: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}\n")
      }
   }
 }
}
