package org.foo

def buildDockerImage(dockerImage){
  try{
       sh """ docker build -t ${dockerImage}:${env.BUILD_NUMBER} -t ${dockerImage}:latest . """
  }

  catch(Exception e) {
    e.printStackTrace()
    throw e;
    }
}

