package org.foo

def deployToKubernetes(){
  try{
    sh """ IMAGE_TAG=${BUILD_NUMBER}  PODS=${PODS} envsubst < kubernetes/test-qa-preprod/deployment.yml | kubectl --kubeconfig ${KubernetesCredentialFilePath}  apply -f - """
  }
  catch(Exception e){
    e.printStackTrace()
    throw e;
  }
}
