package org.foo

def pushDockerImage(dockerImage,dockerRepository,dockerRepoCredentialId){
  try{
    withDockerRegistry([ credentialsId: "${dockerRepoCredentialId}", url: "https://${dockerRepository}" ]) {
              sh "docker push ${dockerImage}:${BUILD_NUMBER}"
              sh "docker push ${dockerImage}:latest"
         }
  }
  catch(Exception e){
    e.printStackTrace()
    throw e;
    }
}
